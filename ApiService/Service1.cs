﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Timers;

namespace ApiService
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            string directory = ConfigurationManager.AppSettings["path"];
            CreateDirectories();
            ProcessDirectory(directory);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 60000; //number in milisecinds  
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
        }

        private void OnElapsedTime(object source, System.Timers.ElapsedEventArgs e)
        {
            string directory = ConfigurationManager.AppSettings["path"];
            CreateDirectories();
            ProcessDirectory(directory);
        }
        static void WriteLogs(string text, string filename)
        {
            string pathLog = ConfigurationManager.AppSettings["path"] + "\\Error";
            var path = string.Format("{0}\\Error-{1}.log", pathLog, filename);
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd.MM.yyyy hh24:mm:ss tt")));
                writer.Close();
            }
        }
        public static void CreateDirectories()
        {
            string path = ConfigurationManager.AppSettings["path"];
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string errorpath = ConfigurationManager.AppSettings["path"] + "\\Error";
            if (!Directory.Exists(errorpath))
            {
                Directory.CreateDirectory(errorpath);
            }
            string receivedpath = ConfigurationManager.AppSettings["path"] + "\\Received";
            if (!Directory.Exists(receivedpath))
            {
                Directory.CreateDirectory(receivedpath);
            }

        }
        public static void ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory, "*.json");
            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName);
            }
        }

        // Insert logic for processing found files here.
        public static void ProcessFile(string path)
        {
            try
            {
                var jsonfile = JObject.Parse(File.ReadAllText(path));
                bool haserror = false;
                using (StreamReader r = new StreamReader(path))
                {
                    string jsonstring = r.ReadToEnd();
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(jsonfile["PO_Number"].ToString().ToUpper() != "CONSG"?ConfigurationManager.AppSettings["ApiUrl"]: ConfigurationManager.AppSettings["ApiConsignedUrl"]);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    //If Po Number is not null and CONSG then call Po Receving Api
                    if (!string.IsNullOrWhiteSpace(jsonfile["PO_Number"].ToString()) && jsonfile["PO_Number"].ToString().ToUpper() != "CONSG")
                    {                        
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string json = new JavaScriptSerializer().Serialize(new
                            {
                                ipkeyUnique = jsonfile["Unique_ID"]!=null?jsonfile["Unique_ID"].ToString():"",
                                Part_no = jsonfile["SEM_PN"] != null ? jsonfile["SEM_PN"].ToString():"",
                                Revision = "",
                                QtyPerPackage = jsonfile["Quantity"] != null ? jsonfile["Quantity"].ToString():"",
                                Mfgr_pt_no = jsonfile["Manufacturer_PN"] != null ? jsonfile["Manufacturer_PN"].ToString():"",
                                Manufacturer_Name = jsonfile["Manufacturer_Name"] != null ? jsonfile["Manufacturer_Name"].ToString():"",
                                Reference = jsonfile["Date_Code"] != null ? jsonfile["Date_Code"].ToString():"",
                                LotCode = jsonfile["Lot_Code"] != null ? jsonfile["Lot_Code"].ToString():"",
                                SupName = jsonfile["Supplier"] != null ? jsonfile["Supplier"].ToString():"",
                                ExpDateFrom = jsonfile["Initialized_Date"] != null ? jsonfile["Initialized_Date"].ToString():"",
                                recPklNo = jsonfile["Packing_Slip_Num"] != null ? jsonfile["Packing_Slip_Num"].ToString():"",
                                dockDate = jsonfile["Packing_Slip_Date"] != null ? jsonfile["Packing_Slip_Date"].ToString():"",
                                PoNum = jsonfile["PO_Number"] != null ? jsonfile["PO_Number"].ToString():"",
                                ApiKey = ConfigurationManager.AppSettings["ApiKey"].ToString()                                
                            });
                            streamWriter.Write(json);
                        }                       
                    }
                    //Consigned
                    else if(!string.IsNullOrWhiteSpace(jsonfile["PO_Number"].ToString()) && jsonfile["PO_Number"].ToString().ToUpper() == "CONSG")
                    {
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string json = new JavaScriptSerializer().Serialize(new
                            {
                                ipkeyUnique = jsonfile["Unique_ID"] != null ? jsonfile["Unique_ID"].ToString():"",
                                CustPartNo = jsonfile["SEM_PN"] != null ? jsonfile["SEM_PN"].ToString():"",
                                CustRev = "",
                                QtyPerPackage = jsonfile["Quantity"] != null ? jsonfile["Quantity"].ToString():"",
                                Mfgr_pt_no = jsonfile["Manufacturer_PN"] != null ? jsonfile["Manufacturer_PN"].ToString():"",
                                Manufacturer_Name = jsonfile["Manufacturer_Name"] != null ? jsonfile["Manufacturer_Name"].ToString():"",
                                Warehouse = jsonfile["Warehouse"] != null ? jsonfile["Warehouse"].ToString() : "",
                                Location = jsonfile["Location"] != null ? jsonfile["Location"].ToString() : "",
                                LotCode = jsonfile["Lot_Code"] != null ? jsonfile["Lot_Code"].ToString():"",
                                Reference = jsonfile["Date_Code"] != null ? jsonfile["Date_Code"].ToString():"",
                                ExpDateFrom = jsonfile["Initialized_Date"] != null ? jsonfile["Initialized_Date"].ToString():"",
                                CustName = jsonfile["Supplier"] != null ? jsonfile["Supplier"].ToString():"",                                
                                recPklNo = jsonfile["Packing_Slip_Num"] != null ? jsonfile["Packing_Slip_Num"].ToString():"",
                                dockDate = jsonfile["Packing_Slip_Date"] != null ? jsonfile["Packing_Slip_Date"].ToString():"",
                                PoNum = "",
                                ApiKey = ConfigurationManager.AppSettings["ApiKey"].ToString()
                            });
                            streamWriter.Write(json);
                        }
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        JObject jsonresult = JObject.Parse(result);
                        //if code is 200(success) file is moved to Received directory
                        if (jsonresult["Code"].ToString() == "200")
                        {
                            haserror = false;
                        }
                        //if code is not 200 (error) error log is created
                        else
                        {
                            haserror = true;
                            WriteLogs(string.Format("Error in file '{0}' at {1}.{2} {3}", path, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), System.Environment.NewLine, jsonresult["Message"].ToString()), Path.GetFileNameWithoutExtension(path));
                        }
                    }
                }
                if (haserror)
                {
                    string errorLog = ConfigurationManager.AppSettings["path"] + "\\Error";
                    var errorpath = errorLog + "\\" + Path.GetFileName(path);
                    if (File.Exists(errorpath))
                        File.Delete(errorpath);
                    File.Move(path, errorpath);
                }
                else
                {
                    string receivedpathLog = ConfigurationManager.AppSettings["path"] + "\\Received";
                    var receivedpath = string.Format("{0}\\{1}", receivedpathLog, Path.GetFileName(path));
                    if (File.Exists(receivedpath))
                        File.Delete(receivedpath);
                    File.Move(path, receivedpath);
                }
            }
            catch (Exception ex)
            {
                WriteLogs(string.Format("Error in file '{0}' at {1}.{2} {3}", path, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), System.Environment.NewLine, ex.ToString()), Path.GetFileNameWithoutExtension(path));
                string errorLog = ConfigurationManager.AppSettings["path"] + "\\Error";
                var errorpath = errorLog + "\\" + Path.GetFileName(path);
                if (File.Exists(errorpath))
                    File.Delete(errorpath);
                File.Move(path, errorpath);

            }
        }
    }
}
